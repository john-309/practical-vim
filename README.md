# Practical-Vim

Source code exercises for the following book:

[Practical Vim - Second Edition](https://pragprog.com/book/dnvim2/practical-vim-second-edition)  

The master branch contains all the stock exercise files that are used within the 
book, except for tips 1 - 36 which have already been worked on within the master 
branch. The rest of the exercise files that I have worked on are under the
"practice" branch. This way I can keep the stock files under the master branch,
in case I need to revert back to the original files.  

